import requests
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import time

lookfor = 'rel=\'USD,EUR,1,2\'>'
myLen = len(lookfor)
plt.ion()
fig = plt.figure()
axes = fig.gca()
plt.minorticks_on
axes.grid(True)
fig.show()
init = True
while True:
    p=requests.get('http://www.xe.com/currencyconverter/convert/?Amount=1&From=USD&To=EUR#converter')
    ind = p.text.find(lookfor)
    myRate = float(p.text[ind+myLen:ind+myLen+7])
    if init:
        myHistory = np.ones(100) * myRate
        init = False
    myHistory = np.delete(myHistory,0)
    myHistory = np.append(myHistory,myRate)
    print(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    #print(myHistory)
    plt.cla()
    plt.minorticks_on
    axes.grid(True)
    axes.plot(myHistory, 'ro-')
    fig.canvas.draw()
    time.sleep(30)
print("Done")

